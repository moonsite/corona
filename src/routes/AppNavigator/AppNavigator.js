import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Routes from '../../utils/Routes';
import {Main, Report} from '../../screens';

export default createAppContainer(
  createStackNavigator(
    {
      [Routes.Screens.MAIN.routeName]: {
        screen: Main,
      },
      [Routes.Screens.REPORT.routeName]: {
        screen: Report,
      },
    },
    {
      initialRouteName: Routes.Screens.MAIN.routeName,
    },
  ),
);
