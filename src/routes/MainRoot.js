import React, { Component } from "react";
import {StyleSheet, View} from 'react-native';
import AppNavigator from './AppNavigator';

export default class MainRoot extends Component {

    render() {
        return (
            <View style={s.container}>
                <AppNavigator />
            </View>
        );
    }
}

const s = StyleSheet.create({
    container: {
      flex: 1,
    },
  });