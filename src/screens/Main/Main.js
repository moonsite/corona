import React, { Component } from "react";
import {StyleSheet, View, Text, Button} from 'react-native';
import Routes from '../../utils/Routes';
  
export default class Main extends Component {

    render() {
        return (
            <View style={{flex: 1}}>
                <Text style={{fontSize: 20}}>Main Screen</Text>
                <Button title='REPORT' onPress={() => this.props.navigation.navigate(Routes.Screens.REPORT.routeName)} />
            </View>
        );
    }
}