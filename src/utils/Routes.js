const Routes = {
    Navigators: {
        APP_NAVIGATOR: {routeName: 'App Navigator'}
    },
    Screens: {
        REPORT: {routeName: 'Report Screen'},
        MAIN: {routeName: 'Main Screen'}
    }
}

export default Routes;